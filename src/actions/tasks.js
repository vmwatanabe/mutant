const axios = require('axios')
const {orderArrayOfObjectsByProp} = require('../utils/array')

function getUsers () {
  return axios.get('https://jsonplaceholder.typicode.com/users')
    .then(response => {
      const users = response.data
      return users
    })
    .catch(error => {
      console.log(error);
    });
}

// mapeia os websites de todos os usuários
function getWebsites () {
  const users = getUsers()
    .then(users => {
      return users.map(elem => ({name: elem.name, website: elem.website}))
    })

  return users
}

// mapeia o nome, email e a empresa, e depois ordena por nome de usuário
function getNameEmailAndCompany () {
  const users = getUsers()
    .then(users => {
      users = users.map(elem => ({
        name: elem.name,
        email: elem.email,
        company: elem.company && elem.company.name ? elem.company.name : '-'
      }))

      users = orderArrayOfObjectsByProp(users, 'name')

      return users
    })

  return users
}

// retorna apenas os usuários que contém a palavra enviada como resposta
function getUsersWithWordInAddress (word) {
  const users = getUsers()
    .then(users => {
      users = users.filter(user => {
        const regex = new RegExp(word)

        if (user.address) {
          for (let i in user.address) {
            let currentAddressProp = user.address[i]
            if (typeof currentAddressProp === 'string' && regex.test(currentAddressProp.toLowerCase())) {
              return true
            }
          }
        }
        return false
      })

      return users
    })

  return users
}

module.exports = {
  getWebsites,
  getNameEmailAndCompany,
  getUsersWithWordInAddress
}
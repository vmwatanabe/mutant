function orderArrayOfObjectsByProp (arr, prop) {
  arr.sort((a,b) => {
    if (a[prop] < b[prop]) return -1
    if (a[prop] > b[prop]) return 1
    return 0
  })
  return arr
}

module.exports = {
  orderArrayOfObjectsByProp
}
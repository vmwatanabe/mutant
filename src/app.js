const express = require('express')
const path = require('path')
const hbs = require('hbs')
const routes = require(path.join(__dirname, '/routes'))

const app = express()
const publicFolderPath = path.join(__dirname, '../public')
const viewsPath = path.join(__dirname, '../templates/views')
const partialsPath = path.join(__dirname, '../templates/partials')

app.set('view engine', 'hbs')
app.set('views', viewsPath)
hbs.registerPartials(partialsPath)

app.use(express.static(publicFolderPath))
app.use(routes)

app.listen(3000, () => {
  console.log('servidor on!')
})
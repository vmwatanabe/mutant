const app = module.exports = require('express')()
const path = require('path')
const hbs = require('hbs')
const viewsPath = path.join(__dirname, '../../templates/views')
const partialsPath = path.join(__dirname, '../../templates/partials')

const elasticsearch = require('elasticsearch')
const client = new elasticsearch.Client({
  host: 'localhost:9200'
});

const {getWebsites, getNameEmailAndCompany, getUsersWithWordInAddress} = require('../actions/tasks')

app.set('view engine', 'hbs')
app.set('views', viewsPath)
hbs.registerPartials(partialsPath)

app.get('', (req, res) => {
  // salvamento de log no elasticsearch
  client.index({
    index: 'mutant',
    type: 'index',
    body: {
      ...req.body,
      description: 'get home',
      timestamp: Date.now()
    }
  })
  res.render('index')
})

app.get('/parte1', (req, res) => {
  // salvamento de log no elasticsearch
  client.index({
    index: 'mutant',
    type: 'index',
    body: {
      ...req.body,
      description: 'get parte 1',
      timestamp: Date.now()
    }
  })
  getWebsites()
    .then((response) => {
      return res.render('parte1', {users: response})
    })
})

app.get('/parte2', (req, res) => {
  // salvamento de log no elasticsearch
  client.index({
    index: 'mutant',
    type: 'index',
    body: {
      ...req.body,
      description: 'get parte 2',
      timestamp: Date.now()
    }
  })
  getNameEmailAndCompany()
    .then((response) => {
      return res.render('parte2', {users: response})
    })
})

app.get('/parte3', (req, res) => {
  // salvamento de log no elasticsearch
  client.index({
    index: 'mutant',
    type: 'index',
    body: {
      ...req.body,
      description: 'get parte 3',
      timestamp: Date.now()
    }
  })
  getUsersWithWordInAddress('suite')
    .then((response) => {
      return res.render('parte3', {users: response})
    })
})

app.get('*', (req, res) => {
  res.render('404', {
      title: '404',
      errorMessage: 'Page not found.'
  })
})

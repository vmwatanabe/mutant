## DESAFIO MUTANT
**por VICTOR MATOS WATANABE**

Olá, avaliadores! Primeiramente, obrigado pelo seu tempo!

Este é a minha aplicação para o desafio do processo seletivo! Obrigado pela oportunidade

O processo de criação da aplicação em si foi tranquilo, porém, a parte de docker/vagrant eu encontrei dificuldades. Este teste me ajudou muito a correr atrás de conteúdo destas tarefas, já que previamente nunca havia trabalhado com docker ou vagrant. Considero o resultado satisfatório.

Consegui rodar no docker usando os comandos:

*$ docker build -t node-mutant .*

*$ docker run -it -p 9003:3000 node-mutant*

No vagrant, não consegui rodar.

Não foram implementados testes automatizados.
